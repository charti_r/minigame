//
// minigame.hpp for minigame in /home/charti_r/Programming/minigame/header
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Tue Mar 11 10:12:09 2014 charti
// Last update Tue Mar 25 16:28:27 2014 charti
//

#ifndef MINIGAME_H_
# define MINIGAME_H_

/*
** Std Include.
*/
# include <vector>
# include <string>
# include <iostream>
# include <fstream>
# include <SFML/Graphics.hpp>
# include <SFML/Audio.hpp>

/*
** My Include.
*/
# include "my.hpp"
# include "constante.hpp"

#endif /* !MINIGAME_H_ */
