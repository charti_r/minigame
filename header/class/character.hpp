//
// character.hpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Sat Mar 15 10:36:47 2014 charti
// Last update Tue Mar 25 17:48:29 2014 charti
//

#ifndef CHARACTER_H_
# define CHARACTER_H_

# include "minigame.hpp"
# include "entity.hpp"
# include "lvl.hpp"

class			Character : public Entity
{
public:
  Character(void);
  Character(const std::string & name);

  void			setName(const std::string & name);
  const std::string &	getName(void) const;
  void			move(int & key, const sf::Time & time, const Lvl & lvl);

private:
  sf::SoundBuffer	m_soundBuffer[2];
  sf::Sound		m_sound[2];
  std::string		m_name;
};

#endif /* !CHARACTER_H_ */
