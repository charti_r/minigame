//
// monster.hpp for minigame in /home/charti_r/Programming/minigame/source/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Tue Mar 25 13:57:35 2014 charti
// Last update Tue Mar 25 15:07:15 2014 charti
//

#ifndef MONSTER_H_
# define MONSTER_H_

# include "minigame.hpp"
# include "entity.hpp"
# include "lvl.hpp"

class			Monster : public Entity
{
public:
  Monster(void);

  void			move(const sf::Time & time, const Lvl & lvl);
}

#endif /* !MONSTER_H_ */
