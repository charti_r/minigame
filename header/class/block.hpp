//
// block.hpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Fri Mar 21 11:40:13 2014 charti
// Last update Wed Apr  2 11:00:17 2014 charti
//

#ifndef BLOCK_H_
# define BLOCK_H_

# include "minigame.hpp"
# include "entity.hpp"

class			Block
{
public:
  Block();
  Block(const int id, const sf::Vector2f & position, const bool isCarried = true);

  int			getId(void) const;
  const sf::Vector2f &	getPosition(void) const;
  bool			hitbox(const sf::Vector2f & pos,
			       const sf::Vector2f & size) const;

protected:
  int			m_id;
  bool			m_isCarried;
  sf::Vector2f		m_position;
  sf::Vector2f		m_size;
};

#endif /* !BLOCK_H_ */
