//
// lvl.hpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Fri Mar 21 09:38:44 2014 charti
// Last update Tue Mar 25 09:05:33 2014 charti
//

#ifndef LVL_H_
# define LVL_H_

# include "minigame.hpp"
# include "block.hpp"

class				Lvl
{
public:
  Lvl();

  int				loadFromFile(const std::string & file);
  int				loadError(const std::string & file) const;
  int				getNbBlock(void) const;
  const Block &			getBlock(int nb) const;
  const sf::Vector2f &		hitbox(const sf::Vector2f & pos,
				       const sf::Vector2f & size) const;

private:
  std::string			m_name;
  std::vector<Block>		m_lvl;
  sf::Vector2f			m_size;
};

#endif /* !LVL_H_ */
