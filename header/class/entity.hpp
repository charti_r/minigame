//
// entity.hpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Wed Mar 12 10:41:49 2014 charti
// Last update Tue Mar 25 18:03:57 2014 charti
//

#ifndef ENTITY_H_
# define ENTITY_H_

# include "minigame.hpp"

class			Entity
{
public:
  Entity();
  Entity(const sf::Vector2f & size, const int life = 100, const int damage = 0);

  bool			isAlive(void) const;
  int			getLife(void) const;
  void			setLife(const int life);
  void			takeDamage(const int damage);
  void			giveDamage(Entity & target);
  void			setDamage(const int damage);
  int			getDamage(void) const;
  void			setSpeed(const sf::Vector2f & speed);
  void			setSpeed(const float speed_x, const float speed_y);
  const sf::Vector2f &	getSpeed(void) const;
  const sf::Vector2f &	getMove(void) const;
  int			isMoving(void) const;
  bool			isFlying(void) const;
  void			setPosition(const sf::Vector2f & position);
  void			setPosition(const int position_x, const int position_y);
  const sf::Vector2f &	getPosition(void) const;
  void			setSize(const sf::Vector2f & size);
  void			setSize(const int width, const int height);
  const sf::Vector2f &	getSize(void) const;

protected:
  int			m_life;
  bool			m_alive;
  sf::Vector2f		m_speed;
  sf::Vector2f		m_move;
  int			m_fly;
  int			m_damage;
  sf::Vector2f		m_position;
  sf::Vector2f		m_size;
};

#endif /* !ENTITY_H_ */
