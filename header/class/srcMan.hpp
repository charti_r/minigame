//
// srcMan.hpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Mon Mar 17 11:00:12 2014 charti
// Last update Tue Mar 25 18:46:45 2014 charti
//

#ifndef SRC_MAN_H_
# define SRC_MAN_H_

# include "minigame.hpp"
# include "lvl.hpp"
# include "character.hpp"

class			SrcMan
{
public:
  SrcMan();

  int			loadLvl(const std::string & lvl_name);
  void			setTextureSprite(void);
  void			update(sf::RenderWindow & window,
			       const Entity & hero, const Lvl & lvl);


private:
  sf::Clock		m_clock;

  sf::Music		m_music;

  sf::Texture		m_sky;
  sf::Sprite		m_sky_sprite;

  sf::Texture		m_text_hero;
  sf::Sprite		m_sprite_hero;
  sf::Texture		m_text_item[nbItem];
  sf::Sprite		m_sprite_item;
  sf::Texture		m_text[nbText];
  sf::Sprite		m_sprite_text[nbText];
};

#endif /* !SRC_MAN_H_ */
