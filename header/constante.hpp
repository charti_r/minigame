//
// constante.hpp for minigame in /home/charti_r/Programming/minigame/header
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Tue Mar 11 10:23:11 2014 charti
// Last update Tue Mar 25 11:52:17 2014 charti
//

#ifndef CONSTANTE_H_
# define CONSTANTE_H_

/*
** Win.
*/
# define WD		1280
# define HT		768
# define WIN_NAME	"Mini Game"

/*
** Const.
*/
# define ANIME_TIME	0.105
# define G		2000
# define SPEED_X	250
# define SPEED_Y	590

/*
** Enum.
*/
enum	e_key
{
  k_left = 0,
  k_right,
  k_space,
  nbKeyHook
};

enum	e_item
{
  heart_1 = 0,
  heart_2,
  heart_3,
  heart_4,
  grave,
  nbItem
};

enum	e_text
{
  stone = 0,
  brick_1,
  brick_2,
  brick_3,
  brick_dark,
  bonus,
  nbText
};

#endif /* !CONSTANTE_H_ */
