//
// entity.cpp for minigame in /home/charti_r/Programming/minigame/source/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Wed Mar 12 10:41:12 2014 charti
// Last update Tue Mar 25 18:19:59 2014 charti
//

#include "entity.hpp"

Entity::Entity()
{
  m_life = 100;
  m_alive = true;
  m_speed = sf::Vector2f(0, 0);
  m_move = sf::Vector2f(0, 0);
  m_fly = 0;
  m_damage = 0;
  m_position = sf::Vector2f(0, 0);
  m_size = sf::Vector2f(0, 0);
}

Entity::Entity(const sf::Vector2f & size, const int life, const int damage)
{
  m_life = life;
  m_alive = true;
  m_speed = sf::Vector2f(0, 0);
  m_move = sf::Vector2f(0, 0);
  m_fly = 0;
  m_damage = damage;
  m_position = sf::Vector2f(0, 0);
  m_size = size;
}

bool	Entity::isAlive(void) const
{
  return (m_alive);
}

int	Entity::getLife(void) const
{
  return (m_life);
}

void	Entity::setLife(const int life)
{
  m_life = life;
  if (life > 0)
    m_alive = true;
}

void	Entity::takeDamage(const int damage)
{
  m_life = ((m_life > damage) ? (m_life - damage) : 0);
  m_alive = (m_life ? true : false);
}

void	Entity::giveDamage(Entity & target)
{
  target.takeDamage(m_damage);
}

void	Entity::setDamage(const int damage)
{
  m_damage = damage;
}

int	Entity::getDamage(void) const
{
  return (m_damage);
}

void	Entity::setSpeed(const sf::Vector2f & speed)
{
  m_speed = speed;
}

void	Entity::setSpeed(const float speed_x, const float speed_y)
{
  Entity::setSpeed(sf::Vector2f(speed_x, speed_y));
}

const sf::Vector2f &	Entity::getSpeed(void) const
{
  return (m_speed);
}

const sf::Vector2f &	Entity::getMove(void) const
{
  return (m_move);
}

int	Entity::isMoving(void) const
{
  return ((!m_move.x) ? 0 : ((m_move.x > 0) ? 1 : -1));
}

bool	Entity::isFlying(void) const
{
  return (m_fly ? true : false);
}

void	Entity::setPosition(const sf::Vector2f & position)
{
  m_position = position;
}

void	Entity::setPosition(const int position_x, const int position_y)
{
  Entity::setPosition(sf::Vector2f(position_x, position_y));
}

const sf::Vector2f &	Entity::getPosition(void) const
{
  return (m_position);
}

void	Entity::setSize(const sf::Vector2f & size)
{
  m_size = size;
}

void	Entity::setSize(const int width, const int height)
{
  Entity::setSize(sf::Vector2f(width, height));
}

const sf::Vector2f &	Entity::getSize(void) const
{
  return (m_size);
}
