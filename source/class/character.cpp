//
// character.cpp for minigame in /home/charti_r/Programming/minigame/source/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Sat Mar 15 10:36:32 2014 charti
// Last update Tue Mar 25 18:43:43 2014 charti
//

#include "character.hpp"

Character::Character(void) : Entity::Entity(sf::Vector2f(52, 74))
{
  m_name = "Calvin";
  m_soundBuffer[0].loadFromFile("sound/sound.wav");
  m_soundBuffer[1].loadFromFile("sound/oNo.wav");
  m_sound[0].setBuffer(m_soundBuffer[0]);
  m_sound[1].setBuffer(m_soundBuffer[1]);
  m_sound[0].setVolume(75);
  m_sound[1].setVolume(150);
}

Character::Character(const std::string & name) : Entity::Entity(sf::Vector2f(52, 74))
{
  m_name = name;
  m_soundBuffer[0].loadFromFile("sound/sound.wav");
  m_soundBuffer[1].loadFromFile("sound/oNo.wav");
  m_sound[0].setBuffer(m_soundBuffer[0]);
  m_sound[1].setBuffer(m_soundBuffer[1]);
  m_sound[0].setVolume(75);
  m_sound[1].setVolume(150);
}

void	Character::setName(const std::string & name)
{
  m_name = name;
}

const std::string &	Character::getName(void) const
{
  return (m_name);
}

void	Character::move(int & key, const sf::Time & time, const Lvl & lvl)
{
  static int	dir = 0;
  sf::Vector2f	block;
  float		pos;
  float		jump;

  m_move.x = 0;
  if (!m_fly)
    dir = 0;
  if (key & (1 << k_left))
    {
      m_move.x += time.asSeconds() * (-m_speed.x);
      dir = -1;
    }
  if (key & (1 << k_right))
    {
      m_move.x += time.asSeconds() * m_speed.x;
      dir = 1;
    }
  if (m_fly && !m_move.x)
    m_move.x = time.asSeconds() * ((m_speed.x * dir) / 2);
  pos = m_position.x + m_move.x;
  block = lvl.hitbox(sf::Vector2f(pos, m_position.y), m_size);
  if (block.x == -1)
    m_position.x = pos;
  if (key & (1 << k_space) && m_fly < 2)
    {
      key &= ~(1 << k_space);
      m_move.y = -m_speed.y;
      if (m_fly)
      	m_move.y *= 0.85;
      ++m_fly;
    }
  jump = m_position.y + time.asSeconds() * (((G * time.asSeconds()) / 2) + m_move.y);
  m_move.y += G * time.asSeconds();
  block = lvl.hitbox(sf::Vector2f(m_position.x, jump), m_size);
  if (block.y == -1)
    m_position.y = jump;
  else
    {
      if (m_move.y > 0)
	{
	  m_fly = 0;
	  m_position.y = block.y - m_size.y;
	  if (m_move.y > 1000)
	    {
	      m_sound[0].play();
	      Entity::takeDamage((m_move.y - 1000) / 10);
	      if (!m_alive)
		{
		  while (m_sound[0].getStatus());
		  m_sound[1].play();
		}
	    }
	}
      else
      	m_position.y = block.y + 64;
      m_move.y = 0;
    }
}
