//
// block.cpp for minigame in /home/charti_r/Programming/minigame/header/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Fri Mar 21 14:05:33 2014 charti
// Last update Tue Mar 25 09:20:14 2014 charti
//

#include "block.hpp"

Block::Block()
{
  m_id = 0;
  m_isCarried = true;
  m_position = sf::Vector2f(0, 0);
  m_size = sf::Vector2f(0, 0);
}


Block::Block(const int id, const sf::Vector2f & position, const bool isCarried)
{
  m_id = id;
  m_isCarried = isCarried;
  m_position = position;
  m_size = sf::Vector2f(64, 64);
}

int	Block::getId(void) const
{
  return (m_id);
}

const sf::Vector2f &	Block::getPosition(void) const
{
  return (m_position);
}

bool	Block::hitbox(const sf::Vector2f & pos,
				      const sf::Vector2f & size) const
{
  if (!m_isCarried)
    return (false);
  if (pos.x > m_position.x)
    {
      if (m_position.x + m_size.x > pos.x)
	if (pos.y > m_position.y)
	  {
	    if (m_position.y + m_size.y > pos.y)
	      return (true);
	  }
	else
	  {
	    if (pos.y + size.y > m_position.y)
	      return (true);
	  }
    }
  else
    {
      if (pos.x + size.x > m_position.x)
	if (pos.y > m_position.y)
	  {
	    if (m_position.y + m_size.y > pos.y)
	      return (true);
	  }
	else
	  {
	    if (pos.y + size.y > m_position.y)
	      return (true);
	  }
    }
  return (false);
}
