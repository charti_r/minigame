//
// lvl.cpp for minigame in /home/charti_r/Programming/minigame/source/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Mon Mar 17 12:55:00 2014 charti
// Last update Tue Mar 25 19:03:48 2014 charti
//

#include "lvl.hpp"

Lvl::Lvl()
{
  m_name = "";
  m_size = sf::Vector2f(0, 0);
}

int		Lvl::loadFromFile(const std::string & file)
{
  std::ifstream	stream;
  std::string	line;
  char		*str;
  int		id;
  sf::Vector2f	pos;
  int		i;

  m_lvl.clear();
  stream.open(file.c_str());
  std::getline(stream, line);
  str = (char*)line.c_str();
  m_size.x = my_getnbr(str);
  i = 0;
  while (str[i] != ' ')
    ++i;
  m_size.y = my_getnbr(&str[++i]);
  std::getline(stream, m_name);
  while (std::getline(stream, line))
    {
      str = (char*)line.c_str();
      id = my_getnbr(str);
      i = 0;
      while (str[i])
	{
	  while (str[i] != ' ' && str[i])
	    ++i;
	  if (!str[i])
	    return (1);
	  pos.x = my_getnbr(&str[++i]);
	  while (str[i] != ';' && str[i])
	    ++i;
	  if (!str[i])
	    return (1);
	  pos.y = my_getnbr(&str[++i]);
	  m_lvl.push_back(Block(id, pos));
	  while ((str[i] >= '0' && str[i] <= '9') || str[i] == '\n')
	    ++i;
	}
    }
  return (0);
}

int	Lvl::loadError(const std::string & file) const
{
  std::cout << "Error loading the file " << file << std::endl;
  return (1);
}

int	Lvl::getNbBlock(void) const
{
  return (m_lvl.size());
}

const Block &	Lvl::getBlock(int nb) const
{
  return (m_lvl[nb]);
}

const sf::Vector2f &	Lvl::hitbox(const sf::Vector2f & pos,
				    const sf::Vector2f & size) const
{
  int	nb_block;
  int	i;

  nb_block = m_lvl.size();
  i = 0;
  while (i < nb_block)
    {
      if (m_lvl[i].hitbox(pos, size))
	return (m_lvl[i].getPosition());
      ++i;
    }
  return (sf::Vector2f(-1, -1));
}
