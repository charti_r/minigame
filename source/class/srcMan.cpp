//
// srcMan.cpp for minigame in /home/charti_r/Programming/minigame/source/class
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Mon Mar 17 10:59:28 2014 charti
// Last update Tue Mar 25 18:50:41 2014 charti
//

#include "srcMan.hpp"

SrcMan::SrcMan()
{
  m_sky.loadFromFile("image/background/sky.png");
  m_sky_sprite.setTexture(m_sky);
  m_sky_sprite.setPosition(0, 0);
  m_text_hero.loadFromFile("image/entity/mario.png");
  m_text_item[heart_1].loadFromFile("image/item/heart_full.png");
  m_text_item[heart_2].loadFromFile("image/item/heart_mid.png");
  m_text_item[heart_3].loadFromFile("image/item/heart_last.png");
  m_text_item[heart_4].loadFromFile("image/item/heart_empty.png"); 
  m_text_item[grave].loadFromFile("image/item/grave.png");
  m_text[stone].loadFromFile("image/block/stone.png");
  m_text[brick_1].loadFromFile("image/block/brick_1.png");
  m_text[brick_2].loadFromFile("image/block/brick_2.png");
  m_text[brick_3].loadFromFile("image/block/brick_3.png");
  m_text[brick_dark].loadFromFile("image/block/brick_dark.png");
  m_text[bonus].loadFromFile("image/block/bonus.png");
  m_clock.restart();
}

void		SrcMan::setTextureSprite(void)
{
  int		i;

  m_sprite_hero.setTexture(m_text_hero);
  m_sprite_hero.setTextureRect(sf::IntRect(52,0, 52, 74));
  i = -1;
  while (++i < nbText)
    m_sprite_text[i].setTexture(m_text[i]);
}

void	SrcMan::update(sf::RenderWindow & window,
		       const Entity & hero, const Lvl & lvl)
{
  static int	step = 0;
  sf::Vector2f	size;
  Block		block;
  int		i;

  window.draw(m_sky_sprite);
  if (hero.isAlive())
    {
      size = hero.getSize();
      if (m_clock.getElapsedTime().asSeconds() > ANIME_TIME)
	{
	  m_clock.restart();
	  if (hero.isFlying())
	    {
	      if (hero.isMoving() == 1)
		m_sprite_hero.setTextureRect(sf::IntRect((int)size.x << 1, 0,
							 size.x, size.y));
	      else
		m_sprite_hero.setTextureRect(sf::IntRect(0, 0,
							 size.x, size.y));
	    }
	  else if (!hero.isMoving())
	    {
	      step = 0;
	      m_sprite_hero.setTextureRect(sf::IntRect(size.x, 0,
						       size.x, size.y));
	    }
	  else
	    {
	      step = (step + 1) % 3;
	      if (hero.isMoving() == 1)
		m_sprite_hero.setTextureRect(sf::IntRect(size.x * step, (int)size.y << 1,
							 size.x, size.y));
	      else
		m_sprite_hero.setTextureRect(sf::IntRect(size.x * step, size.y,
							 size.x, size.y));
	    }
	}
      m_sprite_hero.setPosition(hero.getPosition());
      window.draw(m_sprite_hero);
    }
  else
    {
      m_sprite_item.setTexture(m_text_item[grave]);
      m_sprite_item.setPosition(hero.getPosition().x, hero.getPosition().y + 22);
      window.draw(m_sprite_item);
    }
  i = 0;
  while (i < lvl.getNbBlock())
    {
      block = lvl.getBlock(i);
      m_sprite_text[block.getId()].setPosition(block.getPosition());
      window.draw(m_sprite_text[block.getId()]);
      ++i;
    }
  if (hero.getLife() > 75)
    m_sprite_item.setTexture(m_text_item[heart_1]);
  else if (hero.getLife() > 50)
    m_sprite_item.setTexture(m_text_item[heart_2]);
  else if (hero.getLife() > 25)
    m_sprite_item.setTexture(m_text_item[heart_3]);
  else
    m_sprite_item.setTexture(m_text_item[heart_4]);
  m_sprite_item.setPosition(64, 0);
  window.draw(m_sprite_item);
}
