//
// main.cpp for minigame in /home/charti_r/Programming/minigame/source
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Tue Mar 11 10:11:30 2014 charti
// Last update Fri Apr 25 11:21:21 2014 charti
//

#include "minigame.hpp"
#include "srcMan.hpp"
#include "lvl.hpp"
#include "character.hpp"
#include <unistd.h>

void	key_press(const int keycode, int & key);
void	key_release(const int keycode, int & key);

int		main(void)
{
  sf::RenderWindow	window;
  sf::Event		event;
  sf::Clock		clock;
  sf::Music		music;
  int			key;
  SrcMan		srcMan;
  Lvl			lvl;
  Character		mario;

  if (!music.openFromFile("sound/music.ogg"))
    return (1);
  music.setLoop(true);
  music.setVolume(40);
  if (lvl.loadFromFile("lvl/one.lvl"))
    return (lvl.loadError("lvl/one.lvl"));
  srcMan.setTextureSprite();
  key = 0;
  mario.setPosition(WD >> 1, 629);
  mario.setSpeed(SPEED_X, SPEED_Y);
  mario.setSize(52, 74);
  window.create(sf::VideoMode(WD, HT, 32), WIN_NAME);
  window.setFramerateLimit(60);
  music.play();
  while (window.isOpen())
    {
      while (window.pollEvent(event))
	{
	  switch (event.type)
	    {
	    case sf::Event::Closed:
	      window.close();
	      break;
	    case sf::Event::KeyPressed:
	      key_press(event.key.code, key);
	      break;
	    case sf::Event::KeyReleased:
	      switch (event.key.code)
		{
		case sf::Keyboard::Escape:
		  window.close();
		default:
		  key_release(event.key.code, key);
		  break;
		}
	      break;
	    default:
	      break;
	    }
	}
      window.clear();
      if (mario.isAlive())
	{
	  mario.move(key, clock.getElapsedTime(), lvl);
	  clock.restart();
	  if (mario.getPosition().x > 1216)
	    {
	      if (lvl.loadFromFile("lvl/two.lvl"))
		return (lvl.loadError("lvl/two.lvl"));
	      mario.setPosition(WD >> 1, 629);
	      mario.setLife(100);
	    }
	  
	}
      else
	music.stop();
      srcMan.update(window, mario, lvl);
      window.display();
    }
  return (0);
}

void	key_press(const int keycode, int & key)
{
  switch (keycode)
    {
    case sf::Keyboard::Left:
      key |= 1 << k_left;
      break;
    case sf::Keyboard::Right:
      key |= 1 << k_right;
      break;
    case sf::Keyboard::Space:
      key |= 1 << k_space;
      break;
    default:
      break;
    }
}

void	key_release(const int keycode, int & key)
{
  switch (keycode)
    {
    case sf::Keyboard::Left:
      key &= ~(1 << k_left);
      break;
    case sf::Keyboard::Right:
      key &= ~(1 << k_right);
      break;
    case sf::Keyboard::Space:
      key &= ~(1 << k_space);
      break;
    default:
      break;
    }
}
