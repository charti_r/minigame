//
// libmy.cpp for libmy in /home/charti_r/Programming/minigame/source
// 
// Made by charti
// Login   <charti_r@epitech.net>
// 
// Started on  Sat Mar 22 15:24:01 2014 charti
// Last update Sat Mar 22 15:24:05 2014 charti
//

#include "my.hpp"

int	my_getnbr(char *str)
{
  int	nb;
  int	isneg;
  int	i;

  isneg = 1;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    {
      if (str[i] == '-')
	isneg = -isneg;
      ++i;
    }
  nb = 0;
  while (str[i] >= '0' && str[i] <= '9')
    {
      nb *= 10;
      nb += str[i] - '0';
      ++i;
    }
  return (nb * isneg);
}
