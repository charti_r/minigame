##
## Makefile for minigame in /home/charti_r/Programming/minigame
## 
## Made by charti
## Login   <charti_r@epitech.net>
## 
## Started on  Tue Mar 11 10:10:27 2014 charti
## Last update Fri Apr 18 11:31:49 2014 charti
##

CPPFLAGS	+= -W -Wall -pedantic
CPPFLAGS	+= -I./header/ -I./header/class

CC		= g++
RM		= rm -f

NAME		= minigame

LIB		= -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system

SRC_DIR		= source/
CLASS_DIR	= $(SRC_DIR)class/

SRC		= $(SRC_DIR)main.cpp \
		  $(SRC_DIR)libmy.cpp \
		  $(CLASS_DIR)srcMan.cpp \
		  $(CLASS_DIR)lvl.cpp \
		  $(CLASS_DIR)block.cpp \
		  $(CLASS_DIR)entity.cpp \
		  $(CLASS_DIR)character.cpp

OBJ		= $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
